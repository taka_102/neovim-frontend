# Neovim config

I've been using vim for quite sometime now and i am pretty much in love with it.
But let's be honest, it's quite hard to get started with it and build your own IDE with it.
This is where this repository comes into the picture.

## Dependencies

Before cloning the repository make sure you have neovim and node js installed first.
To install neovim just go their website or github repository and once you have that, add it to your PATH (environment variables).

Next we need to intall nodejs.
After run the following command:

```
npm install -g typescript typescript-language-server diagnostic-languageserver eslint_d prettier
```

after node installed, you need to install packer, which is a package manager for neovim.
just go to their github repository and follow the inscrutions

## Install my neovim config

To install my configuration files is pretty simple, just replace your neovim config folder content by mine.
To see where your config folder is just type the following command in your neovim.

```
:echo stdpath('config')
```

Once the repository cloned, open neovim and run the followinghtml:

```
:PackerInstall
```

And then restart neovim
