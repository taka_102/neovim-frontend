local keymap = vim.api.nvim_set_keymap

local opts = {noremap=true, silent=true}

local function format(...)
	return string.format(...)
end

-- editor keymap
keymap('n', '<leader>h', '<C-w>h', {noremap=true, silent=true})
keymap('n', '<leader>l', '<C-w>l', {noremap=true, silent=true})
keymap('n', '<leader>j', '<C-w>j', {noremap=true, silent=true})
keymap('n', '<leader>k', '<C-w>k', {noremap=true, silent=true})
keymap('n', '<Space>s', '<cmd>w<cr>', opts)
keymap('n', '<Space>w', '<cmd>bd<cr>', opts)

-- nerd tree keymap
keymap('n', '<space>e', '<cmd>NvimTreeToggle<cr>', opts)


-- telescope key binding
local builtins = "<cmd>lua require('telescope.builtin')"
local theme = "require('telescope.themes').get_ivy()"
vim.keymap.set('n', '<space>ff', function()
	local builtin = require('telescope.builtin')
	local ivy = require('telescope.themes').get_ivy()
	local success = pcall(function() builtin.git_files(ivy) end)
	if not success then
		builtin.find_files(ivy)
	end
end)
keymap('n', '<space>fa',format('%s.find_files(%s)<cr>', builtins, theme), opts)
keymap('n', '<space>fts', format('%s.live_grep()<cr>', builtins), opts)
keymap('n', '<space>fe', ':Telescope file_browser<cr>',opts)
keymap('n', '<space>bl', format('%s.buffers(%s)<cr>', builtins, theme), opts)

-- tabs
keymap('n', '<space>tn', ':tabnext<cr>', opts)
keymap('n', '<space>tp', ':tabprev<cr>', opts)

-- luasnip
local ls = "<cmd>lua require('luasnip')"
keymap( 'i', '<c-j>', ls..'.jump(1)<cr>', {silent=true})

-- tabline
-- keymap('n', '<space>l', ':BufferLineCycleNext<cr>', {silent =true, noremap = true})
-- keymap('n', '<space>h', ':BufferLineCyclePrev<cr>', {silent =true, noremap = true})


-- nvim-dap
vim.keymap.set("n", "<F5>", ":lua require'dap'.continue()<CR>")
vim.keymap.set("n", "<F10>", ":lua require'dap'.step_over()<CR>")
vim.keymap.set("n", "<F11>", ":lua require'dap'.step_info()<CR>")
vim.keymap.set("n", "<F12>", ":lua require'dap'.step_out()<CR>")
vim.keymap.set("n", "<leader>b", ":lua require'dap'.toggle_breakpoint()<CR>")
vim.keymap.set("n", "<space>lp", ":lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>")
