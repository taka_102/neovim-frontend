local group = vim.api.nvim_create_augroup('BaseEditorAutoCmd', {clear = true})
-- vim.api.nvim_create_autocmd('BufEnter', { command = 'set foldmethod=indent', group  = group})
vim.api.nvim_create_autocmd('BufEnter', { command = 'sign define DiagnosticSignError text= texthl=DiagnosticSignError linehl= numhl=', group=group })
vim.api.nvim_create_autocmd('BufEnter', { command = 'sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl=', group=group })
vim.api.nvim_create_autocmd('BufEnter', { command = 'sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo linehl= numhl=', group=group })
vim.api.nvim_create_autocmd('BufEnter', { command = 'sign define DiagnosticSignHint text= texthl=DiagnosticSignHint linehl= numhl=', group=group })


-- word wrap

