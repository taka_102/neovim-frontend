vim.o.encoding = 'UTF-8'
vim.o.showmatch	= true		-- show matching
vim.o.ignorecase = true		-- case inensitive
vim.o.mouse='a'			-- middle-click paste
vim.o.hlsearch = true			-- highlight search
vim.o.incsearch	= true		-- incremental search
vim.o.tabstop= 4			-- number of columns occupied by a tab
vim.o.softtabstop = 4		-- see multiple spaces as tabstops
-- vim.o.expandtab	 = true		-- converts tabs to white space
vim.o.shiftwidth = 4		-- width for autoindent
vim.o.autoindent =true		-- indent a new line the same amount as le line just typed
vim.o.number = true			-- show line number
vim.o.relativenumber= true
vim.o.wildmode='longest,list'	-- get bash-like tab completions
--vim.o.cc = '80'			-- vim.o.an 90 column border for good coding
-- filetype plugin indent on	-- allow auto-indent depending on file type
-- syntax on 			-- syntax highlight
-- vim.o.mouse='a'			-- enable  mouse click
-- vim.o.clipboard=unnamedplus	-- using system clipboard filetype plugin on
vim.o.cursorline = true			-- highlight current cursorline
vim.o.ttyfast = true			-- speed up scrolling
-- vim.o.spell	= true		-- enable spell check (may need to download package)
vim.o.swapfile = false			-- disable creating swap file
-- vim.o.backupdir=D:/.nvim/backup	" directory to store backup files
vim.o.title = true
vim.o.termguicolors = true
vim.o.guifont = "Iosevka SS14"
vim.o.cmdheight = 0

vim.opt.winblend = 0
vim.opt.pumblend = 0

