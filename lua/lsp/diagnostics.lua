local nvim_lsp = require('lspconfig')
local lsp_conf = require('lsp.config.lsp')


nvim_lsp.diagnosticls.setup {
	on_attach = lsp_conf.on_attach,
	filetypes = {'javascript', 'javascriptreact', 'typescript', 'typescriptreact', 'json', 'css', 'less', 'scss', 'markdown', 'pandoc' },
	init_options = {
		linters = {
			eslint = {
				command = 'eslint_d',
				rootPatterns = { '.git' },
				debounce = 100,
				args = { '--stdin', '--stdin-filename', '%filepath%', '--format', 'json' },
				sourceName = 'eslint_d',
				parseJson = {
					errorsRoot = '[0].messages',
					line = 'line',
					column = 'column',
					endLine = 'endLine',
					endColumn = 'endColumn',
					message = '[eslint] ${message} [${ruleId}]',
					security = 'severity'
				},
				securities = {
					[2] = 'error',
					[1] = 'warning'
				}
			}
		},

		filetypes = {
			javascript = 'eslint',
			javascriptreact = 'eslint',
			typescript = 'eslint',
			typescriptreact = 'eslint',
			vue = 'eslint',
		},
		formatters = {
			eslint_d = {
				command = 'eslint_d',
				args = { '--stdin', '--stdin-filename', '%filename', '--fix-to-stdout' },
				rootPatterns = { '.git' },
			},
			prettier = {
				command = 'prettier',
				args = { '--stdin-filepath', '%filename' }
			}
		},
		formatFiletypes = {
			html = 'prettier',
			htmldjango = 'prettier',
			css = 'prettier',
			javascript = 'prettier',
			javascriptreact = 'prettier',
			scss = 'prettier',
			less = 'prettier',
			typescript = 'prettier',
			typescriptreact = 'prettier',
			json = 'prettier',
			markdown = 'prettier',
			vue = 'prettier'
		}
	}
}

