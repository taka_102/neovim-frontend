local saga = require('lspsaga')
vim.keymap.set('n', 'K', '<cmd>Lspsaga hover_doc<cr>', { silent = true })

saga.setup{
	border_style = 'rounded',
};
