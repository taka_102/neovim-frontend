require 'neoconf'.setup({})
require 'neodev'.setup({})
local nvim_lsp = require 'lspconfig'
local config = require 'lsp.config.lsp'

local capabilities = config.capabilities


nvim_lsp.intelephense.setup {
	on_attach = config.on_attach,
	capabilities = capabilities,
	flags = {
		debounce_text_changes = 150,
	}
}

nvim_lsp.volar.setup {
	on_attach = config.on_attach,
	capabilities = capabilities,
	flags = {
		debounce_text_changes = 150,
	},
	filetypes = {'vue'}
}

nvim_lsp.pyright.setup {
	on_attach = config.on_attach,
	capabilities = capabilities,
	flags = {
		debounce_text_changes = 150,
	}
}

nvim_lsp.gopls.setup {
	on_attach = config.on_attach,
	capabilities = capabilities,
	flags = {
		debounce_text_changes = 150,
	}
}

nvim_lsp.kotlin_language_server.setup{}
nvim_lsp.tailwindcss.setup{}



-- config for deno
vim.g.markdown_fenced_languages = {
  "ts=typescript"
}
nvim_lsp.denols.setup {
	on_attach = config.on_attach,
	capabilities = config.capabilities,
	root_dir = nvim_lsp.util.root_pattern('deno.json', 'deno.jsonc'),
	flags = {
		debounce_text_changes = 150,
	}
}

nvim_lsp.tsserver.setup {
	on_attach = config.on_attach,
	capabilities = config.capabilities,
	root_dir = nvim_lsp.util.root_pattern('package.json'),
	flags = {
		debounce_text_changes = 150,
	}
}


nvim_lsp.emmet_ls.setup({
    -- on_attach = on_attach,
    capabilities = capabilities,
    filetypes = { 'html', 'typescriptreact', 'javascriptreact',
		'css', 'sass', 'scss', 'less', 'twig', 'htmldjango', 'tsx', 'jsx'},
    init_options = {
      html = {
        options = {
          ["bem.enabled"] = true,
        },
      },
    }
})

nvim_lsp.html.setup {
	capabilities = capabilities,
	filetypes = {	'html', 'htmldjango', 'twig' },
	{
		configurationSection = { "html", "css", "javascript" },
		embeddedLanguages = {
			css = true,
			javascript = true
		},
		provideFormatter = true
	}
}


nvim_lsp.lua_ls.setup({
	settings = {
		Lua = {
		  completion = {
			callSnippet = "Replace"
		  }
		}
	  }
})

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")
require('lspconfig').lua_ls.setup {
  -- on_attach = function(client, buff)
  --   config.on_attach(client, buff)
  --   vim.cmd [[autocmd BufWritePre <buffer> lua require'stylua-nvim'.format_file()]]
  -- end,
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
        path = runtime_path,
      },
      diagnostics = {
        globals = { 'vim' },
      },
      workspace = {
        library = vim.api.nvim_get_runtime_file('', true),
        checkThirdParty = false, -- THIS IS THE IMPORTANT LINE TO ADD
      },
      telemetry = {
        enable = false,
      },
    },
  },
}

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
	vim.lsp.handlers.hover, { border = "rounded", }
)

local border = {
      {"🭽", "FloatBorder"},
      {"▔", "FloatBorder"},
      {"🭾", "FloatBorder"},
      {"▕", "FloatBorder"},
      {"🭿", "FloatBorder"},
      {"▁", "FloatBorder"},
      {"🭼", "FloatBorder"},
      {"▏", "FloatBorder"},
}
return config
