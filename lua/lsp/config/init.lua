M = {}


M.load = function ()
	require('lsp.config.completion')
	require('lsp.config.dap')
end

return M
