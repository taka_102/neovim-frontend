vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'

	-- theme
	use 'sainnhe/sonokai'
	use 'sainnhe/gruvbox-material'
	use 'rmehri01/onenord.nvim'
	use 'morhetz/gruvbox'
	use 'sainnhe/everforest'
	use 'projekt0n/github-nvim-theme'
	use 'Mofiqul/vscode.nvim'
	use { "2nthony/vitesse.nvim", requires = { "tjdevries/colorbuddy.nvim" } }
	use { "catppuccin/nvim", as = "catppuccin" }
	use 'folke/tokyonight.nvim'

	use { 'TimUntersberger/neogit', requires = 'nvim-lua/plenary.nvim' }
	use 'lewis6991/gitsigns.nvim'
	use 'imsnif/kdl.vim'


	use "xiyaowong/nvim-transparent"

	-- debugging
	use 'mfussenegger/nvim-dap'
	use 'rcarriga/nvim-dap-ui'
	use 'theHamsta/nvim-dap-virtual-text'
	use 'leoluz/nvim-dap-go'


	-- layouts and helpers
	use { 'kyazdani42/nvim-tree.lua', requires = { 'kyazdani42/nvim-web-devicons',  }, tag = 'nightly'}
	-- use 'kdheepak/tabline.nvim'
	use 'akinsho/bufferline.nvim'
	use {
		'nvim-lualine/lualine.nvim',
		requires = {'kyazdani42/nvim-web-devicons', opt = true}
	}
	use {'nvim-telescope/telescope.nvim',requires = { {'nvim-lua/plenary.nvim'} } }
	use { "nvim-telescope/telescope-file-browser.nvim" }
	use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
	use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
	-- indentation
	use "lukas-reineke/indent-blankline.nvim"

	-- lsp config related plugins
	use 'neovim/nvim-lspconfig'
	use 'glepnir/lspsaga.nvim'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-cmdline'
	use 'hrsh7th/nvim-cmp'
	use 'L3MON4D3/LuaSnip'
	use 'saadparwaiz1/cmp_luasnip'
	use 'onsails/lspkind.nvim'
	use { 'jose-elias-alvarez/null-ls.nvim', requires = { "nvim-lua/plenary.nvim" },}

	-- neovim builtin auto
	use "folke/neodev.nvim"
	use "folke/neoconf.nvim"


	-- utils pakages
	use "fladson/vim-kitty"
	-- color highlight
	-- use 'norcalli/nvim-colorizer.lua'
	use 'NvChad/nvim-colorizer.lua'
	-- comment nvim
	use 'numToStr/Comment.nvim'
	use 'folke/todo-comments.nvim'

	use 'ray-x/go.nvim'
	use 'mattn/emmet-vim'

	-- twig
	use 'nelsyeung/twig.vim'
	use 'nickeb96/fish.vim'

	-- kotlin support
	use 'udalov/kotlin-vim'

end)
