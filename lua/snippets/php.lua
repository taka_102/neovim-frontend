local ls = require "luasnip"
local s = ls.s
local i = ls.insert_node
local f = ls.function_node
local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local lowerFirstLetter = function(index) 
	return f(function(args)
		local name = args[1][1] -- The name of the variable
		firstLetter = string.sub(name, 1, 1)
		firstLetter = string.lower(firstLetter)
		return firstLetter .. string.sub(name, 2, -1)
	end, {index})
end

ls.add_snippets("php", {
	s("method", fmt([[
		public function {}({})
		{{
			{}
		}}
	]], { i(1), i(2), i(0) })),
	s("getter", fmt([[
		public function get{}()
		{{
			return $this->{};
		}}
	]], { i(1), lowerFirstLetter(1) })),
	s("setter", fmt([[
		public function set{}($value)
		{{
			$this->{} = $value;
		}}
	]], { i(1), lowerFirstLetter(1) })),

})


ls.add_snippets("twig", {
	s("block", fmt("{{% block {} %}}{}{{% endblock %}}", { i(1), i(0) })),
	s("inclue", fmt("{{% include '{}' %}}", {i(0)})),
	s("asset", fmt("{{{{ asset('{}') }}}}", {i(0)})),
	s("extends", fmt("{{% extends '{}' %}}", { i(0) })),
	s("if", fmt([[
		{{% if {} %}}
			{}
		{{% endif %}}
	]], { i(1), i(2) })),
	s("fn", fmt("{{% {} %}}", { i(1) })),
})

