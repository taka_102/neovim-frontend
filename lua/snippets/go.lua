local ls = require('luasnip')
local s = ls.s
local i = ls.insert_node
local fmt = require('luasnip.extras.fmt').fmt



ls.add_snippets("go", {
	-- snippet for function
	s("fun", fmt([[
		func {}({}){{
			{}
		}}
		]], { i(1), i(2), i(0) })),

		-- snippet for methods
		s("method", fmt([[
		func ({}) {}({}){{
			{}
		}}
		]], { i(1), i(2), i(3), i(0) })),

		-- snippet for handler
		s("handler", fmt([[
		func {}(w http.ResponseWriter, r *http.Request){{
			{}
		}}
		]], { i(1), i(0) })),

		-- snippet for method handlers
		s("mhandler", fmt([[
		func ({}) {}(w http.ResponseWriter, r *http.Request){{
			{}
		}}
		]], { i(1), i(2), i(0) })),
	})
