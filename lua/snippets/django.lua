
local ls = require "luasnip"
local s = ls.s
local i = ls.insert_node
local f = ls.function_node
local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local lowerFirstLetter = function(index) 
	return f(function(args)
		local name = args[1][1] -- The name of the variable
		firstLetter = string.sub(name, 1, 1)
		firstLetter = string.lower(firstLetter)
		return firstLetter .. string.sub(name, 2, -1)
	end, {index})
end


ls.add_snippets("htmldjango", {
	s("block", fmt("{{% block {} %}}{}{{% endblock %}}", { i(1), i(0) })),
	s("inclue", fmt("{{% include '{}' %}}", {i(0)})),
	s("extends", fmt("{{% extends '{}' %}}", { i(0) })),
	s("if", fmt([[
		{{% if {} %}}
			{}
		{{% endif %}}
	]], { i(1), i(2) })),
	s("fn", fmt("{{% {} %}}", { i(1) })),
})

