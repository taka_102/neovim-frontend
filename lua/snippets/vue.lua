local ls = require('luasnip')
local s = ls.s
local i = ls.insert_node

local fmt = require('luasnip.extras.fmt').fmt

local vuesnp = {
	s({trig='sfcts', dscr='generate a vue sfc component with typescript'}, {
	fmt([[
		<script setup lang="ts">
		{}
		</script>

		<template>
		</template>
	]], {i(0)})
	})
}



ls.add_snippets("vue", {
	s({trig='sfcts', name='generate a vue sfc component with typescript'}, 
	fmt([[
		<script setup lang="ts">
		{}
		</script>

		<template>
		</template>
	]], {i(0)})
	)
})
