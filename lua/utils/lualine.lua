local lualine = require 'lualine'
local theme = require 'lualine.themes.nightfly'

local mods = {
	n = 'normal',
	i = 'insert',
	v = 'visual',
	[''] = 'visual',
	V = 'visual',

}


lualine.setup {
	options = {
		theme = 'auto',
		component_separators = '',
		section_separators = { left = '', right = '' },
		-- component_separators = { left = '', right = ''},
		-- section_separators = { left = '', right = ''},
		globalstatus=true,
	},
	sections = {
		lualine_a = {
			{
				'mode', 
				fmt = function (str) return '' end,
				separator = {left = '', right = '' },
				right_padding = 10,
				left_padding = 10
			}
		},
		lualine_b = {
			 {
                "diagnostics",
                sources =  {"nvim_lsp"},
                sections = {"error", "warn", "info", "hint"},
                diagnostics_color = {
                    error = { fg = "#fb617e" },
                    warn  = { fg = "#edc763" },
                    info  = { fg = "#6dcae8" },
                    hint  = { fg = "#ffffff" }
                },
                symbols = {
                    error = " ",
                    warn  = " ",
                    info  = " ",
                    hint  = " "
                }
            },
            {
                "diff",
                colored = true,
                diff_color = {
                    added    = { fg = "#a9dc76" },
                    modified = { fg = "#edc763" },
                    removed  = { fg = "#fb617e" }
                },
                symbols = {
                    added    = " ",
                    modified = " ",
                    removed  = " "
                }
            }
	},
		lualine_c = {
			'%=' ,
			{
				'filename',

			},
		},
		lualine_y = {
			{
				"branch",
				icon = "",
				separator = { left = '', right = '' }
			},
		}
	}
}
