-- nvim tree
vim.g.nvim_tree_quit_on_open = 1
vim.g.nvim_tree_group_empty = 1
require'nvim-tree'.setup{
	auto_close = true,
	view = {
		side = "down",
		width = 30
	}
}


-- tabline
-- require'tabline'.setup{ }
-- require("bufferline").setup{
-- 	options = {
-- 		mode = "tabs"
-- 	}
-- }


-- telescope
require'telescope'.setup {
	defaults = {  file_ignore_patterns = {"node_modules", "vendor", "build", ".vscode", ".idea"} }
}
require 'telescope'.load_extension "file_browser"
-- require 'telescope'.load_extension 'fzf'

-- treesitter
require('nvim-treesitter.configs').setup{
	require'nvim-treesitter.configs'.setup {
		--  ensure_installed = { "c", "lua", "rust" },
		-- sync_install = false,
		-- ignore_install = { "javascript" },

		highlight = {
			enable = true,
			disable = { "c", "rust" },
			additional_vim_regex_highlighting = false,
		},
	}
}

-- comment nvim
require('Comment').setup()

-- indent
-- vim.opt.list = true
-- vim.opt.listchars:append("space:⋅")
-- vim.opt.listchars:append("eol:↴")
-- vim.opt.listchars:append("tab:⇝ ")

-- require("indent_blankline").setup {
-- 	show_end_of_line = true,
-- 	space_char_blankline = " ",
-- }


-- transparency
require("transparent").setup({
	enable = false

})
