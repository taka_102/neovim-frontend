M = {}

M.nighlify = {
  color3   = '#2c3043',
  color6   = '#a1aab8',
  color7   = '#82aaff',
  color8   = '#ae81ff',
  color0   = '#092236',
  color1   = '#ff5874',
  color2   = '#c3ccdc',
}


return M
