vim.g.sonokai_transparent_background = true
vim.cmd 'colorscheme sonokai'

require('colorizer').setup {
	user_default_options = {
		tailwind = true,
	}
}


