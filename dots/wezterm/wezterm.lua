local wezterm = require 'wezterm'
local env = require 'config.env'

wezterm.color.load_scheme(env.path .. '/colors/vitesse.toml')


return {
	font_size = 11,
	font = wezterm.font {
		family = 'JetbrainsMono Nerd Font',
		harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' },
	},
	-- color_scheme = 'Vitesse',
	color_scheme= 'Monokai Soda',
	window_background_opacity = 1.0,
	hide_tab_bar_if_only_one_tab = true,
	window_decorations = 'NONE',
}
